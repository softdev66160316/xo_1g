import java.util.Scanner;

public class XO1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to XO game");
        System.out.println("Start");

        char[][] board = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}
        };
        char player = 'X';
        boolean Won = false;

        while (!Won) {
            printBoard(board);
            System.out.println("Turn "+player);
            System.out.print("Please enter your number of (roll) :");
            int row = scanner.nextInt()-1;
            System.out.print("Please enter your number of (column) :");
            int column = scanner.nextInt()-1;

            if (row >= 0 && row < 3 && column >= 0 && column < 3 && board[row][column] == '-'){
                board[row][column] = player;
                Won = checkWinner(board, player);
                player = (player == 'X') ? 'O' : 'X';
            }
            else {
                System.out.println("This position is already taken or invalid. Try again.");
            }
        }
        printBoard(board);
        if (Won){
            System.out.println("Player "+(player == 'X' ? 'O' : 'X')+" Win !!!");
        }
        else{
            System.out.println("It's a tie!");
        }
    }
    public static boolean checkWinner(char[][] board,char player){
        for (int i = 0;i<3;i++){
            if (board[i][0] == player && board[i][1] == player && board[i][2] == player){
                return true ;
            }
        }
        for (int i = 0;i<3;i++){
            if (board[0][i] == player && board[1][i] == player && board[2][i] == player){
                return true ;
            }
        }
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player){
            return true ;
        }
        if (board[0][2] == player && board[1][1] == player && board[2][0] == player){
            return true ;
        }
        return false;
    }


    public static void printBoard(char[][] board){
        for (int i = 0;i < board.length;i++){
            for (int j = 0;j< board[i].length;j++){
                System.out.print(board[i][j]+" ");
            }
            System.out.println();
        }
    }
}

    

